import 'package:flutter/material.dart';
import 'package:tawtheiq/data/model/dashboard.dart';
import 'package:tawtheiq/repository/dashboard_repository.dart';
class DashboardProvider extends ChangeNotifier {
  Dashboard _dashboard=Dashboard();

  DashboardRepository _dashboardRepository=DashboardRepository();

  Dashboard get  dashboard=> _dashboard;


  set dashboard (Dashboard dashboard){
    _dashboard=dashboard;
    notifyListeners();
  }
  Future<void> fetchDashboard()async{
    Dashboard dash = await _dashboardRepository.fetchDashboardData();
    _dashboard=dash;
    notifyListeners();
  }
}