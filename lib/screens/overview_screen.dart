
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawtheiq/provider/dashboard_provider.dart';
import 'package:tawtheiq/screens/widgets/overview_card.dart';

class OverView extends StatelessWidget {
  const OverView({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Provider.of<DashboardProvider>(context, listen: false).fetchDashboard();
    return Padding(
      padding: const EdgeInsets.only(left: 40,right: 20),
      child:Consumer<DashboardProvider>(builder: (context, dashboard, child){
        return dashboard.dashboard.allItems!=null?
            CustomScrollView(
          slivers: [
            SliverFillRemaining(
              fillOverscroll: false,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 5,),
                  RichText(
                    text: TextSpan(
                      text: 'Hello, ',
                      style: TextStyle(fontFamily: 'DejaVuSans',fontWeight: FontWeight.w700,fontSize: 15,color: Color(0xFF164674)),
                      children: <TextSpan>[
                        TextSpan(text: 'Waseem Bahlol', style: TextStyle(fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300,fontSize: 15,color: Color(0xFF164674))),
                      ],
                    ),
                  ),
                  Text('Art Director at Tawthieq',style: TextStyle(color: Color(0xFF869FB7)),),
                  SizedBox(height: 10,),

                  Text('You have 3 MOMs Pendnig review today',style: TextStyle(color: Color(0xFF164674)),),
                  Divider(color: Color(0xFFEBAC2D),thickness: 5,),
                  SizedBox(height: 5,),
                  Text('You have 2 MOMs Pendnig approval today',style: TextStyle(color: Color(0xFF164674))),
                  Divider(color: Color(0xFFEBAC2D),thickness: 5,),
                  SizedBox(height: 5,),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 20,bottom: 50),
                      child: Column(
                        children: [
                          Expanded(child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Expanded(flex: 2,
                                  child: dashboard.dashboard.allItems.momByStatusList.length>2?
                                  OverviewCard(color:Color(0xFFFDA12D) ,title: dashboard.dashboard.allItems.momByStatusList[2].groupName,count: dashboard.dashboard.allItems.momByStatusList[2].groupItemsCount.toString(),img: 'assets/clock.png',colorAlter:Colors.white ,fontColor: Color(0xFFFDA12D),):
                                  OverviewCard(color:Color(0xFFFDA12D) ,title: 'No Data available',count:'0',img: 'assets/clock.png',colorAlter:Colors.white ,fontColor: Color(0xFFFDA12D),)

                              ),
                              Expanded(flex: 1,
                                  child: dashboard.dashboard.allItems.momByStatusList.length>0?
                                  OverviewCard(color:Color(0xFF707070) ,title: dashboard.dashboard.allItems.momByStatusList[0].groupName,count: dashboard.dashboard.allItems.momByStatusList[0].groupItemsCount.toString(),img: 'assets/doc_done.png',colorAlter: Color(0xFF707070),):
                                  OverviewCard(color:Color(0xFF707070) ,title: 'No Data available',count:'0',img: 'assets/doc_done.png',colorAlter: Color(0xFF707070),)

                              ),
                            ],
                          )),
                          Expanded(child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Expanded(flex: 1,
                                  child: dashboard.dashboard.allItems.momByStatusList.length>4?
                                  OverviewCard(color:Color(0xFFF83131) ,title: dashboard.dashboard.allItems.momByStatusList[4].groupName,count: dashboard.dashboard.allItems.momByStatusList[4].groupItemsCount.toString(),img: 'assets/doc_not.png',colorAlter:Color(0xFFF83131) ):
                                  OverviewCard(color:Color(0xFFF83131) ,title: 'No Data available',count:'0',img: 'assets/doc_not.png',colorAlter:Color(0xFFF83131) )

                              ),
                              Expanded(flex: 2,
                                  child: dashboard.dashboard.allItems.momByStatusList.length>1?
                                  OverviewCard(color:Color(0xFFEBAC2D) ,title: dashboard.dashboard.allItems.momByStatusList[1].groupName ,count: dashboard.dashboard.allItems.momByStatusList[1].groupItemsCount.toString(),img: 'assets/doc_found.png',colorAlter:Color(0xFFEBAC2D) ,):
                                  OverviewCard(color:Color(0xFFEBAC2D) ,title: 'No Data available',count:'0',img: 'assets/doc_found.png',colorAlter:Color(0xFFEBAC2D) ,)

                              ),
                            ],
                          )),
                          Expanded(child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Expanded(
                                  child: dashboard.dashboard.allItems.actionByStatusList.length>2?
                                  OverviewCard(color: Color(0xFF707070),title: dashboard.dashboard.allItems.actionByStatusList[2].groupName,count: dashboard.dashboard.allItems.actionByStatusList[2].groupItemsCount.toString(),img: 'assets/list.png',colorAlter:Color(0xFF707070) ,):
                                  OverviewCard(color: Color(0xFF707070),title: 'No Data available',count:'0',img: 'assets/list.png',colorAlter:Color(0xFF707070) ,)

                              ),
                              Expanded(
                                  child: dashboard.dashboard.allItems.actionByStatusList.length>0?
                                  OverviewCard(color:Color(0xFFF83131),title: dashboard.dashboard.allItems.actionByStatusList[0].groupName,count: dashboard.dashboard.allItems.actionByStatusList[0].groupItemsCount.toString(),img: 'assets/list.png',colorAlter:Color(0xFFF83131) ,):
                                  OverviewCard(color:Color(0xFFF83131),title: 'No Data available',count:'0',img: 'assets/list.png',colorAlter:Color(0xFFF83131) ,)

                              ),
                              Expanded(
                                  child: dashboard.dashboard.allItems.actionByStatusList.length>1?
                                  OverviewCard(color:Color(0xFFFDA12D),title: dashboard.dashboard.allItems.actionByStatusList[1].groupName,count:dashboard.dashboard.allItems.actionByStatusList[1].groupItemsCount.toString(),img: 'assets/list.png',colorAlter:Color(0xFFFDA12D) ,):
                                  OverviewCard(color:Color(0xFFFDA12D),title: 'No Data available',count:'0',img: 'assets/list.png',colorAlter:Color(0xFFFDA12D) ,)
                              ),
                            ],
                          )),
                          Expanded(child: Row(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                          )),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            )
          ],

        )
            :Center(child: Text('loading'));
      }),

    );
  }
}


