import 'package:flutter/material.dart';
import 'package:tawtheiq/screens/widgets/actions_widget.dart';
import 'package:tawtheiq/screens/widgets/moms_widget.dart';

class Details extends StatelessWidget {
  const Details({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 0,
      length: 2,
      child: Padding(
        padding: const EdgeInsets.only(left: 40,right: 20),
        child: Scaffold(
          appBar:AppBar(
            elevation: 0,
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(kToolbarHeight*0.2),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  TabBar(
                      isScrollable: true,
                      indicatorColor: Colors.transparent,
                      labelStyle: TextStyle(fontSize: 22.0,color: Color(0xFF164674)),
                      unselectedLabelStyle: TextStyle(fontSize: 12.0),
                      tabs: [
                        Text('MOMs',style: TextStyle(color: Color(0xFF707070)),),
                        Text('Actions',style: TextStyle(color: Color(0xFF707070)),),
                      ]),
                  DropdownButton(
                    value: 'All MOMs',
                    icon: const Icon(Icons.keyboard_arrow_down ),
                    iconSize: 24,
                    elevation: 16,
                    style: const TextStyle(fontSize: 12,color: Colors.black),
                    underline: SizedBox(),
                    onChanged: (val){},
                    items: <String>['All MOMs', 'Need Review', 'Pending', 'Approved']
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                  )
                ],
              ),
            )
          ),
          body: TabBarView(
                children: [
                  Center(child: MOMWidget()),
                  Center(child: ActionWidget())
                ]),
        ),
      ),
    );
  }
}
