import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:tawtheiq/screens/widgets/statisitc_row.dart';

Map<String, double> dataMap = {
  "Action Not": 26,
  "Action in progress": 54,
  "ActionCompleted": 30,

};
class Statistics extends StatelessWidget {
  const Statistics({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
          elevation: 0,
          backgroundColor: Theme.of(context).scaffoldBackgroundColor,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(kToolbarHeight*0.2),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text('MOMs Statistics',style: TextStyle(fontSize: 15,color: Color(0xFF164674),fontFamily: 'DejaVuSans',fontWeight: FontWeight.w700),),
                DropdownButton(
                  value: 'Last Month',
                  icon: const Icon(Icons.keyboard_arrow_down ),
                  iconSize: 24,
                  elevation: 16,
                  style: const TextStyle(fontSize: 12,color: Colors.black),
                  underline: SizedBox(),
                  onChanged: (val){},
                  items: <String>['Last Month', 'Last Week', 'Yesterday']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )
              ],
            ),
          )
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 40,right: 20),
              child: SingleChildScrollView(
                child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            StatisticRow(title:'MOM Attended (Last Month)' ,count: '30',footer: '+20% since last month',color: Color(0xFF00CE30),),
                            Divider(thickness: 2,),
                            StatisticRow(title:'MOM Approved (Last Month)' ,count: '10',footer: '-15% since last month',color: Color(0xFFCE0F13),),
                            Divider(thickness: 2,),
                            StatisticRow(title:'MOM Reviewed (Last Month)' ,count: '55',footer: '+20% since last month',color: Color(0xFF00CE30),),
                            Divider(thickness: 2,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                CircularPercentIndicator(
                                  radius:MediaQuery.of(context).size.width*0.2,
                                  lineWidth: 10.0,
                                  percent: 0.74,
                                  center: Text('74%'),
                                  progressColor: Color(0xFF0C3C66),
                                  animation: true,
                                  animationDuration: 2000,
                                  header:Text('Attended'),
                                  circularStrokeCap: CircularStrokeCap.round,
                                ),
                                CircularPercentIndicator(
                                  radius:MediaQuery.of(context).size.width*0.2,
                                  lineWidth: 10.0,
                                  percent: 0.25,
                                  center: Text('25%'),
                                  progressColor: Color(0xFFCE0F13),
                                  animation: true,
                                  animationDuration: 2000,
                                  header:Text('Review'),
                                  circularStrokeCap: CircularStrokeCap.round,
                                ),
                                CircularPercentIndicator(
                                  radius:MediaQuery.of(context).size.width*0.2,
                                  lineWidth: 10.0,
                                  percent: 0.80,
                                  center: Text('80%'),
                                  progressColor: Color(0xFF00CE30),
                                  animation: true,
                                  animationDuration: 2000,
                                  header:Text('Approve'),
                                  circularStrokeCap: CircularStrokeCap.round,
                                )
                              ],
                            ),
                            SizedBox(height: 5,),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Text('Actions Statistics',style: TextStyle(fontSize: 15,color: Color(0xFF164674),fontFamily: 'DejaVuSans',fontWeight: FontWeight.w700),),
                                DropdownButton(
                                  value: 'Last Month',
                                  icon: const Icon(Icons.keyboard_arrow_down ),
                                  iconSize: 24,
                                  elevation: 16,
                                  style: const TextStyle(fontSize: 12,color: Colors.black),
                                  underline: SizedBox(),
                                  onChanged: (val){},
                                  items: <String>['Last Month', 'Last Week', 'Yesterday']
                                      .map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                )
                              ],
                            ),
                            PieChart(
                              dataMap: dataMap,
                              animationDuration: Duration(milliseconds: 2000),
                              chartLegendSpacing: 10,
                              chartRadius: MediaQuery.of(context).size.width / 2,
                              colorList: [Color(0xFFCE0F13),Color(0xFF00CE30),Color(0xFF707070)],
                              initialAngleInDegree: 0,
                              chartType: ChartType.ring,
                              ringStrokeWidth: 20,
                              legendOptions: LegendOptions(
                                showLegendsInRow: true,
                                legendPosition: LegendPosition.bottom,
                                showLegends: true,
                                legendShape: BoxShape.circle,
                                legendTextStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              chartValuesOptions: ChartValuesOptions(
                                showChartValueBackground: true,
                                showChartValues: true,
                                showChartValuesInPercentage: false,
                                showChartValuesOutside: true,
                                decimalPlaces: 0,
                              ),
                            ),
                            SizedBox(height: 20,)
                          ],
                        ),
              ),
                 
        ),
    );
  }
}
