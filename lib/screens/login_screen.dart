import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tawtheiq/screens/home_screen.dart';
import 'package:tawtheiq/screens/widgets/login_button.dart';
class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
            image:DecorationImage(
              image: AssetImage("assets/background.png"),
              fit: BoxFit.fill,
            )
        ),
        child:SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child:Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Row(
                          children: [
                            Image(
                              image: AssetImage(
                                'assets/Asset 4.png',
                              ),
                            ),
                            SizedBox(width: 20,),
                            Text('Tawthieq',style: TextStyle(fontSize: 24,color: Colors.white,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w700),)
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text('Hello There',style: TextStyle(fontSize: 20,color: Colors.white,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300),),
                                SizedBox(height: 5,),
                                Text('Welcome On Board',style: TextStyle(fontSize: 20,color: Colors.white,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300),),
                                SizedBox(height: 5,),
                                Text('Stay Up to Meet',style: TextStyle(fontSize: 20,color: Colors.white,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w700),),
                                SizedBox(height: 15,),
                              ],
                            ),

                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 4),
                                        child: ElevatedButton(
                                          onPressed: (){Navigator.push(context, MaterialPageRoute(builder:(context) => HomeScreen()));},
                                          child: Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: Text('Sign In',style: TextStyle(fontSize:15,color:Color(0xFF164674)),),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                              side:BorderSide(
                                                  color: Colors.black.withOpacity(0.5),
                                                  style: BorderStyle.solid
                                              ),
                                              primary: Colors.white,
                                              textStyle: TextStyle(
                                                fontSize: 10,
                                              )),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 4),
                                        child: ElevatedButton(
                                          onPressed: (){},
                                          child: Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: Text('Sign Up',style: TextStyle(fontSize:15),),
                                          ),
                                          style: ElevatedButton.styleFrom(
                                              side:BorderSide(
                                                  color: Colors.white,
                                                  style: BorderStyle.solid
                                              ),
                                              primary: Colors.white.withOpacity(0.1),
                                              textStyle: TextStyle(
                                                fontSize: 10,
                                              )),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Center(child: Text('OR Sign in with',style: TextStyle(fontSize: 18,color: Colors.white,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300),)),
                            Column(

                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Row(
                                  children: [
                                    Expanded(
                                      child: LoginButton(color2:Color(0xFF707070),color1:Color(0xFFFFFFFF),title:'Apple',icon: FaIcon(FontAwesomeIcons.apple),),
                                    ),
                                    Expanded(
                                      child: LoginButton(color2:Color(0xFF4285F4),color1:Color(0xFF4285F4) ,title:'Google' ,icon: FaIcon(FontAwesomeIcons.google),),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                      child: LoginButton(color2:Color(0xFFF13C19),color1:Color(0xFFF13C19) ,title:'Office365' ,icon: Image.asset('assets/office.png',height: 24,),),
                                    ),
                                    Expanded(
                                      child: LoginButton(color2:Color(0xFF0858A7),color1:Color(0xFF0858A7) ,title:'LinkedIn' ,icon: FaIcon(FontAwesomeIcons.linkedin,color: Color(0xFF0B65C2),),),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Text('Login with other',style: TextStyle(fontSize: 12,color: Colors.white,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300,
                            ),),
                          ],
                        ),
                        SizedBox()
                      ],
                    ) ,
                  )
                ]
            ),
          ),
        ),
      ),
    );

  }
}

