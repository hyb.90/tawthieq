import 'package:floating_bottom_navigation_bar/floating_bottom_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawtheiq/provider/bottom_nav_provider.dart';
import 'package:tawtheiq/screens/first_screen.dart';
import 'package:tawtheiq/screens/secone_screen.dart';

List<Widget> currentTab = [
  SecondScreen(),
  SecondScreen(),
  SecondScreen(),
  SecondScreen(),
  FirstScreen(),
];
class HomeScreen extends StatelessWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var navProvider = Provider.of<BottomNavigationBarProvider>(context);
    return Scaffold(
      appBar:AppBar(
        elevation: 0,
        automaticallyImplyLeading: false,
        backgroundColor:Theme.of(context).scaffoldBackgroundColor,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(onPressed: (){}, icon: Image.asset('assets/calendar.png',height: 24)),
                Text('27/7/2021',style: TextStyle(color: Color(0xFF707070),fontSize: 12),)
              ],
            ),
            Image(
              image: AssetImage('assets/Asset_4.png'),height: 35,),
            Row(
              children: [
                IconButton(onPressed: (){}, icon: Image.asset('assets/ring.png',height: 24)),
                IconButton(onPressed: (){}, icon: Image.asset('assets/search.png',height: 24)),
                CircleAvatar(
                  backgroundColor: Color(0xFF164674),
                  radius: kToolbarHeight*0.35,
                  child: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: kToolbarHeight*0.32,
                      child: CircleAvatar(
                        radius: kToolbarHeight*0.27,
                        backgroundColor: Color(0xFF164674),
                        child: Text('WB',style: TextStyle(fontSize: 10),),
                      )
                  ),
                )
              ],
            )
          ],
        ),

      ),
      body:
       Stack(
          children: [
            currentTab[navProvider.currentIndex],
            Positioned(
              bottom: 20,
              left: 5,
              right: 5,
              child: Container(
                child: FloatingNavbar(
                  selectedBackgroundColor: Colors.white.withOpacity(0.5),
                  selectedItemColor: Colors.white.withOpacity(0.5),
                  backgroundColor: Colors.white.withOpacity(0.5),
                  borderRadius: 20,
                  itemBorderRadius: 100,
                  margin: EdgeInsets.only(bottom: 5, top: 5),
                  onTap: (int val) {
                    navProvider.currentIndex = val;
                  },

                  currentIndex: navProvider.currentIndex,
                  items: [
                    FloatingNavbarItem(customWidget: navProvider.currentIndex==0?CircleAvatar(child: Image.asset('assets/apps.png',height: 24,color: Color(0xFF707070),)):CircleAvatar(backgroundColor: Colors.transparent,child: Image.asset('assets/apps.png',height: 24,)),),
                    FloatingNavbarItem(customWidget: navProvider.currentIndex==1?CircleAvatar(child: Image.asset('assets/doc_done2.png',height: 24,color: Color(0xFF707070),)):CircleAvatar(backgroundColor: Colors.transparent,child: Image.asset('assets/doc_done2.png',height: 24,)),),
                    FloatingNavbarItem(customWidget: navProvider.currentIndex==2?CircleAvatar(child: Image.asset('assets/list_no.png',height: 24,color: Color(0xFF707070),)):CircleAvatar(backgroundColor: Colors.transparent,child: Image.asset('assets/list_no.png',height: 24,)),),
                    FloatingNavbarItem(customWidget: navProvider.currentIndex==3?CircleAvatar(child: Image.asset('assets/list_correct.png',height: 24,color: Color(0xFF707070),)):CircleAvatar(backgroundColor: Colors.transparent,child: Image.asset('assets/list_correct.png',height: 24,)),),
                    FloatingNavbarItem(customWidget: navProvider.currentIndex==4?CircleAvatar(child: Image.asset('assets/doc_add.png',height: 24,color: Color(0xFF707070),)):CircleAvatar(backgroundColor: Colors.transparent,child: Image.asset('assets/doc_add.png',height: 24,)),),
                  ],
                ),
              ),
            ),
          ],
        ),



      );

  }
}
