import 'package:flutter/material.dart';
class StatisticRow extends StatelessWidget {
  final String count;
  final String title;
  final String footer;
  final Color color;
  const StatisticRow({
    this.count='',
    this.title='',
    this.footer='',
    this.color=Colors.white,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(count,style: TextStyle(fontSize: 40,color: Color(0xFF164674),fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300),)
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children:[
            Text(title,style: TextStyle(fontSize: 18,color: Color(0xFF707070),fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300),),
            Text(footer,style: TextStyle(fontSize: 18,color: color,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300),)
          ],

        )
      ],
    );
  }
}