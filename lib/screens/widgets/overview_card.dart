import 'package:flutter/material.dart';
class OverviewCard extends StatelessWidget {
  final String img;
  final String title;
  final String count;
  final Color color;
  final Color colorAlter;
  final Color fontColor;

  const OverviewCard({
    Key key,
    this.color=Colors.white,
    this.colorAlter=Colors.white,
    this.fontColor=Colors.white,
    this.img='',
    this.title='',
    this.count=''
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: color, width: 1),
          borderRadius: BorderRadius.circular(10),),
        color: colorAlter,
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Row(),
                  Image.asset(img,height: 50,),
                  Text(title,textAlign: TextAlign.center,style: TextStyle(color: fontColor,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w700,fontSize: 10),),
                  Text(count,textAlign: TextAlign.center,style: TextStyle(color: fontColor,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w700),),
                ],
              ),
            ),
          ],
        )
    );
  }
}