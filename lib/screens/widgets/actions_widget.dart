import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:provider/provider.dart';
import 'package:tawtheiq/provider/dashboard_provider.dart';

class ActionWidget extends StatelessWidget {
  const ActionWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Provider.of<DashboardProvider>(context, listen: false).fetchDashboard();
    return Consumer<DashboardProvider>(builder: (context, dashboard, child){
      return dashboard.dashboard.allItems!=null?
      ListView.builder(
          itemCount: dashboard.dashboard.allItems.actionByStatusList.length,
          itemBuilder: (context,index)=>
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('#'+((index+2)*435246).toString(),style: TextStyle(color: Colors.black),),
                          Text('Pending',style: TextStyle(color: Colors.black),),
                          Row(
                            children: [
                              Text('Public',style: TextStyle(color: Colors.black),),
                              IconButton(onPressed: (){}, icon: Icon(Icons.more_horiz))
                            ],
                          )

                        ],
                      ),
                      Text(dashboard.dashboard.allItems.actionByStatusList[index].groupName),
                      Text(
                          (index+2).toString()+'/07/2021'
                      ),
                      SizedBox(height: 5,),
                      LinearPercentIndicator(
                        width: MediaQuery.of(context).size.width*0.7,
                        animation: true,
                        lineHeight: 5.0,
                        animationDuration: 2500,
                        backgroundColor: Color(0xFF00CE20),
                        percent: dashboard.dashboard.allItems.actionByStatusList[index].groupItemsCount/100,
                        linearStrokeCap: LinearStrokeCap.roundAll,
                        progressColor: Color(0xFFCE0000),
                      ),
                    ],
                  ),
                ),
              ))
          :Text('loading');
    });
  }
}
