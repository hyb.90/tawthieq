import 'package:flutter/material.dart';
class LoginButton extends StatelessWidget {
  final String title;
  final Widget icon;
  final Color color1;
  final Color color2;

  const LoginButton({
    Key key,
    this.title,
    this.icon,
    this.color1,
    this.color2
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: ElevatedButton(
        onPressed: (){},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            icon,
            SizedBox(width: 2,),
            Text(title),
          ],
        ),
        style: ElevatedButton.styleFrom(
            side:BorderSide(
                color: color1,
                style: BorderStyle.solid
            ),
            primary: color2.withOpacity(0.3),
            textStyle: TextStyle(
              fontSize: 12,color: Colors.white,fontFamily: 'DejaVuSans',fontWeight: FontWeight.w300,
            )),
      ),
    );
  }
}