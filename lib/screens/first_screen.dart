import 'package:flutter/material.dart';
import 'package:tawtheiq/screens/details_screen.dart';
import 'package:tawtheiq/screens/overview_screen.dart';
import 'package:tawtheiq/screens/statistic_screen.dart';
class FirstScreen extends StatelessWidget {
  const FirstScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: 2,
      length: 3,
      child: Stack(
        children: [
          TabBarView(
              children: [
                Details(),
                Statistics(),
                OverView(),
              ]),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RotatedBox(
                quarterTurns: 3,
                child: TabBar(
                    isScrollable: true,
                    indicatorColor: Colors.transparent,
                    labelStyle: TextStyle(fontSize: 22.0,color: Color(0xFF164674)),
                    unselectedLabelStyle: TextStyle(fontSize: 12.0,color: Color(0xFF707070)),
                    tabs: [
                      Text('Details',style: TextStyle(color:Color(0xFF707070)),),
                      Text('Statistics',style: TextStyle(color: Color(0xFF707070)),),
                      Text('Overview',style: TextStyle(color: Color(0xFF707070)),),
                    ]),
              )],
          ),

        ],
      ),

    );
  }
}
