import 'package:http/http.dart'as http;
import 'package:tawtheiq/utils/constant.dart';
import 'dart:io';
import 'exception.dart';
import 'dart:convert';
class ApiBaseHelper {

  final String _baseUrl = BaseUrl;
  final String _token = Token;
  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      final response = await http.get(
          Uri.parse(_baseUrl+url),
          headers: {'Authorization':'Bearer $_token'},
      );
      responseJson = _returnResponse(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _returnResponse(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }

  }

}