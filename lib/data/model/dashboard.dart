
class Dashboard {
  AllItems allItems;
  TodayItems todayItems;

  Dashboard({this.allItems, this.todayItems});

  Dashboard.fromJson(Map<String, dynamic> json) {
    allItems = json['allItems'] != null
        ? new AllItems.fromJson(json['allItems'])
        : null;
    todayItems = json['todayItems'] != null
        ? new TodayItems.fromJson(json['todayItems'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.allItems != null) {
      data['allItems'] = this.allItems.toJson();
    }
    if (this.todayItems != null) {
      data['todayItems'] = this.todayItems.toJson();
    }
    return data;
  }
}

class AllItems {
  List<MomByStatusList> momByStatusList;
  List<MomByStatusList> actionByStatusList;

  AllItems({this.momByStatusList, this.actionByStatusList});

  AllItems.fromJson(Map<String, dynamic> json) {
    if (json['momByStatusList'] != null) {
      momByStatusList = <MomByStatusList>[];
      json['momByStatusList'].forEach((v) {
        momByStatusList.add(new MomByStatusList.fromJson(v));
      });
    }
    if (json['actionByStatusList'] != null) {
      actionByStatusList = <MomByStatusList>[];
      json['actionByStatusList'].forEach((v) {
        actionByStatusList.add(new MomByStatusList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.momByStatusList != null) {
      data['momByStatusList'] =
          this.momByStatusList.map((v) => v.toJson()).toList();
    }
    if (this.actionByStatusList != null) {
      data['actionByStatusList'] =
          this.actionByStatusList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MomByStatusList {
  String groupColor='#FFFFFF';
  String groupName;
  int groupType;
  int groupItemsCount;

  MomByStatusList(
      {this.groupColor, this.groupName, this.groupType, this.groupItemsCount});

  MomByStatusList.fromJson(Map<String, dynamic> json) {
    groupColor = json['groupColor'];
    groupName = json['groupName'];
    groupType = json['groupType'];
    groupItemsCount = json['groupItemsCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['groupColor'] = this.groupColor;
    data['groupName'] = this.groupName;
    data['groupType'] = this.groupType;
    data['groupItemsCount'] = this.groupItemsCount;
    return data;
  }
}

class TodayItems {
  List<MomByStatusList> momByStatusList;
  List<MomByStatusList> actionByStatusList;

  TodayItems({this.momByStatusList, this.actionByStatusList});

  TodayItems.fromJson(Map<String, dynamic> json) {
    if (json['momByStatusList'] != null) {
      momByStatusList = <MomByStatusList>[];
      json['momByStatusList'].forEach((v) {
        momByStatusList.add(new MomByStatusList.fromJson(v));
      });
    }
    if (json['actionByStatusList'] != null) {
      actionByStatusList = <MomByStatusList>[];
      json['actionByStatusList'].forEach((v) {
        actionByStatusList.add(new MomByStatusList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.momByStatusList != null) {
      data['momByStatusList'] =
          this.momByStatusList.map((v) => v.toJson()).toList();
    }
    if (this.actionByStatusList != null) {
      data['actionByStatusList'] =
          this.actionByStatusList.map((v) => v.toJson()).toList();
    }
    return data;
  }
}


