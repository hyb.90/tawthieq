import 'package:tawtheiq/data/model/dashboard.dart';
import 'package:tawtheiq/data/services/api_client.dart';

class DashboardRepository {

  ApiBaseHelper _helper = ApiBaseHelper();

  Future<Dashboard> fetchDashboardData() async {
    final response = await _helper.get('/app/dashboard');
    return Dashboard.fromJson(response);
  }
}